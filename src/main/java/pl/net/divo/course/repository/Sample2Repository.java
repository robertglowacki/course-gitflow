package pl.net.divo.course.repository;

import org.springframework.stereotype.Repository;
import pl.net.divo.course.model.Sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class Sample2Repository {
    private static final Map<String, Sample> storage = new HashMap<>();

    public List<Sample> findAll() {
        return new ArrayList<>(storage.values());
    }

    public void save(Sample sample) {
        storage.put(sample.getId(), sample);
    }

    public void delete(String id) {
        storage.remove(id);
    }

    public Sample findById(String id) {
        return storage.get(id);
    }

    public Optional<Sample> findByName(String name) {
        return storage.values().stream()
                .filter(sample -> sample.getName().equals(name))
                .findFirst();
    }

    public void deleteAll() {
        storage.clear();
    }
}
