package pl.net.divo.course.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {
    @GetMapping("/books/isHealthy")
    public String isHealthy() {
        return "OK";
    }
}
