package pl.net.divo.course;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.net.divo.course.model.Sample;
import pl.net.divo.course.repository.SampleRepository;

import java.util.List;

class SampleControllerTest extends AbstractIntegrationTest {
    @Autowired
    SampleRepository sampleRepository;

    @Test
    void shouldNotFindAnySamples() {
        // when
        List<Sample> samples = doGet("/tests", new TypeReference<List<Sample>>() {
        });

        // then
        Assertions.assertEquals(0, samples.size());
    }

    @Test
    void shouldAddTestWithoutErrors() {
        // given
        Sample sample = new Sample("1", "name", "tester");

        // when
        doPost("/tests", sample);

        // then
        Assertions.assertEquals(1, sampleRepository.findAll().size());
        Assertions.assertEquals("name", sampleRepository.findById("1").getName());
        Assertions.assertEquals("tester", sampleRepository.findById("1").getTester());
    }

    @Test
    void shouldFindAllSamples() {
        // given
        doPost("/tests", new Sample("1", "name1", "tester1"));
        doPost("/tests", new Sample("2", "name2", "tester2"));

        // when
        List<Sample> samples = doGet("/tests", new TypeReference<List<Sample>>() {
        });

        // then
        Assertions.assertEquals(2, samples.size());
        Assertions.assertEquals("name1", samples.get(0).getName());
        Assertions.assertEquals("name2", samples.get(1).getName());
    }

    @Test
    void shouldFindById() {
        // given
        doPost("/tests", new Sample("1", "name1", "tester1"));

        // when
        Sample sample = doGet("/tests/1", Sample.class);

        // then
        Assertions.assertEquals("1", sample.getId());
        Assertions.assertEquals("name1", sample.getName());
        Assertions.assertEquals("tester1", sample.getTester());
    }

    @Test
    void shouldFindByName() {
        // given
        doPost("/tests", new Sample("1", "name1", "tester1"));

        // when
        List<Sample> sample = doGet("/tests?byName=name1", new TypeReference<List<Sample>>() {
        });

        // then
        Assertions.assertEquals(1, sample.size());
        Assertions.assertEquals("1", sample.get(0).getId());
        Assertions.assertEquals("name1", sample.get(0).getName());
        Assertions.assertEquals("tester1", sample.get(0).getTester());
    }

    @Test
    void shouldNotFindByNameWhenNotExist() {
        // given
        doPost("/tests", new Sample("1", "name1", "tester1"));

        // when
        List<Sample> sample = doGet("/tests?byName=name2", new TypeReference<List<Sample>>() {
        });

        // then
        Assertions.assertEquals(0, sample.size());
    }

    @Test
    void shouldDeleteSample() {
        // given
        doPost("/tests", new Sample("1", "name1", "tester1"));
        List<Sample> originalSamples = sampleRepository.findAll();

        // when
        doDelete("/tests/1");
        List<Sample> sampleAfterDelete = sampleRepository.findAll();

        // then
        Assertions.assertEquals(1, originalSamples.size());
        Assertions.assertEquals("name1", originalSamples.get(0).getName());
        Assertions.assertEquals(0, sampleAfterDelete.size());
    }

    @AfterEach
    void clean() {
        sampleRepository.deleteAll();
    }
}
